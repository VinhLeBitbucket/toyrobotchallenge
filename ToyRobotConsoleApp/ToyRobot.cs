﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyRobotConsoleApp
{
    public class ToyRobot : IRobot
    {
        private BoardDimension boardDimension;
        private Coordinate currentPosition;
        private bool gotValidPlacement;

        public ToyRobot(BoardDimension bd)
        {
            boardDimension = bd;
            currentPosition = null;
            gotValidPlacement = false;
        }

        private bool isValidPlacement(Coordinate coord)
        {
            bool retVal = false;

            if (coord != null)
            {
                retVal = boardDimension.validCoordinate(coord.X, coord.Y);
            }

            return retVal;          
        }

        public void Place(Coordinate coord)
        {
            if (isValidPlacement(coord))
            {
                gotValidPlacement = true;
                //currentPosition = new Coordinate(coord.X, coord.Y, coord.dir);
                currentPosition = new Coordinate(coord);
            } 
            else if (gotValidPlacement)
            {
                currentPosition = null;
                gotValidPlacement = false;
            }
        }     
        public void Move()
        {
            if (gotValidPlacement)
            {
                Coordinate tempNewPos = new Coordinate(currentPosition);
                switch (currentPosition.dir)
                {
                    case Direction.EAST:
                        tempNewPos.X += 1;
                        break;
                    case Direction.WEST:
                        tempNewPos.X -= 1;
                        break;
                    
                    case Direction.NORTH:
                        tempNewPos.Y += 1;
                        break;
                    case Direction.SOUTH:
                        tempNewPos.Y -= 1;
                        break;
                    default:
                        break;
                }
                if (isValidPlacement(tempNewPos))
                {
                    Place(tempNewPos);
                }
            }
        }

        public void Left()
        {
            if (gotValidPlacement)
            {
                switch (currentPosition.dir)
                {
                    case Direction.NORTH:
                        currentPosition.dir = Direction.WEST;
                        break;
                    case Direction.WEST:
                        currentPosition.dir = Direction.SOUTH;
                        break;
                    case Direction.SOUTH:
                        currentPosition.dir = Direction.EAST;
                        break;
                    case Direction.EAST:
                        currentPosition.dir = Direction.NORTH;
                        break;
                    default:
                        break;
                }
            }
        }


        public void Right()
        {
            if (gotValidPlacement)
            {
                switch (currentPosition.dir)
                {
                    case Direction.NORTH:
                        currentPosition.dir = Direction.EAST;
                        break;
                    case Direction.EAST:
                        currentPosition.dir = Direction.SOUTH;
                        break;
                    case Direction.SOUTH:
                        currentPosition.dir = Direction.WEST;
                        break;
                    case Direction.WEST:
                        currentPosition.dir = Direction.NORTH;
                        break;
                    default:
                        break;
                }
            }
        }

        public void Report()
        {
            if (gotValidPlacement && currentPosition != null)
            {
                Console.WriteLine(currentPosition.X + "," + currentPosition.Y + " " + currentPosition.dir);
            }
        }
    }
}
