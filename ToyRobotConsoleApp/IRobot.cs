﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyRobotConsoleApp
{
    public interface IRobot
    {
        void Place(Coordinate coord);
        void Move();
        void Left();
        void Right();
        void Report();
    }
}
