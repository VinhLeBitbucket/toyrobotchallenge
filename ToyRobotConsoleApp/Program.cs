﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyRobotConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DisplayStuff disp = new DisplayStuff();
            disp.ShowIntroduction();         
            
            string robotCommand = "Vinh Le";
            ParseCommand parser = new ParseCommand();
            BoardDimension bd = new BoardDimension();
            ToyRobot myToyRobot = new ToyRobot(bd);

            while (robotCommand != "q")
            {
                robotCommand = Console.ReadLine();
                parser.StartParsing(robotCommand);
                switch (parser.GetCommand())
                {
                    case "Nothing":
                        break;
                    case "PLACE":
                        Coordinate coord = parser.GetPlaceCommandCoordinate();                       
                        myToyRobot.Place(coord);
                        break;
                    case "MOVE":
                        myToyRobot.Move();
                        break;
                    case "LEFT":
                        myToyRobot.Left();
                        break;
                    case "RIGHT":
                        myToyRobot.Right();
                        break;
                    case "REPORT":
                        myToyRobot.Report();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
