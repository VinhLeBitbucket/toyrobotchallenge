﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyRobotConsoleApp
{
    public class DisplayStuff
    {
        public DisplayStuff()
        {

        }

        public void ShowIntroduction()
        {
            Console.WriteLine("Vinh Le version 0.7 - ToyRobot Challenge!");
            Console.WriteLine(" ");
            Console.WriteLine("1- Commands are NOT case sensitive. They must contain EXACT spacing or  comma format");
            Console.WriteLine("2- Eg: To enter command with format => [PLACE X,Y, Direction] , ignore the square brackets marking ");
            Console.WriteLine("3- Notice no spaces before 'PLACE' or after 'Direction' but exists in between  the comma and before 'Direction' ");
            Console.WriteLine("4- Please refer to online TOYROBOT challenge for details instruction if you are new to this");
            Console.WriteLine("5- To quit, please enter q ");
        }
    }
}
