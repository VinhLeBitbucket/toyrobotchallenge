﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyRobotConsoleApp
{
    public enum Direction
    {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    public class BoardDimension
    {
        uint width;
        uint length;

        public BoardDimension()
        {
            // default
            length = 5;
            width = 5;
        }
        public BoardDimension(uint l, uint w)
        {
            length = l;
            width = w;
        }

        public bool validCoordinate(uint x, uint y)
        {
            bool boolVal = false;

            if ( (x >= 0 && x <= width - 1) && (y >= 0 && y <= length - 1))
            {
                boolVal = true;
            }

            return boolVal;
        }
    }
}
